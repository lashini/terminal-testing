# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.soundrecorder'
        desired_caps['appActivity'] = 'com.android.soundrecorder.SoundRecorder'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def record_01(self):
        record = self.driver.find_element_by_id('com.android.soundrecorder:id/recordButton')
        record.click()

        print 'Success!: Start recording.'
        sleep(6)

        stop = self.driver.find_element_by_id('com.android.soundrecorder:id/stopButton')
        stop.click()
    
        save = self.driver.find_element_by_id('com.android.soundrecorder:id/acceptButton')
        save.click()

        ok = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.Button") and contains(@text, "OK")]')
        ok.click()

        print 'Success!: Save recording.'


    def record_02(self):
        record = self.driver.find_element_by_id('com.android.soundrecorder:id/recordButton')
        record.click()
        sleep(3)

        stop = self.driver.find_element_by_id('com.android.soundrecorder:id/stopButton')
        stop.click()
    
        discard = self.driver.find_element_by_id('com.android.soundrecorder:id/discardButton')
        discard.click()    

        ok = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.Button") and contains(@text, "OK")]')
        ok.click()

        print 'Success!: Discard recording.'



    def testcase_soundRec(self):
        self.record_01()
        self.record_02()



    def tearDown(self):
        self.driver.quit()





        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1109", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1109", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit

