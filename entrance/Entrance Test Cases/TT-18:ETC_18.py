# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.chrome'
        desired_caps['appActivity'] = 'com.google.android.apps.chrome.Main'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def is_connected(self):
        x = True
        countif = 0
        countelif = 0
        while x is True and countif < 5:
            if countif<4:
                try:

                    if (self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@text, "Yahoo")]')). is_displayed() is True:
                        print 'SUCCESS!: Load Yahoo.'
                        sleep(2)
                        x = False
                    else:
                        pass
                    return False
                except:
                    sleep(2)
                    countif = countif + 1
            elif countif is 4 and countelif < 1:
                print 'ERROR!: Trying to connect again'
                countif = 1
                countelif = 1
            else:
                print 'ERROR!: Connection Error'
                countif =countif +1
            #pass

        self.driver.back()

        



    def web_page(self):
        search = self.driver.find_element_by_id('com.android.chrome:id/search_box_text')
        search.click()

        print 'Success!: Default Google search page view.'

        url = self.driver.find_element_by_id('com.android.chrome:id/url_bar')
        url.send_keys("yahoo") 

        self.driver.press_keycode(66)    #Enter
        sleep(10)

        print 'Success!: Google search.'

        self.driver.tap([(85,422)], 500) 
        sleep(6)
        self.is_connected()

        #self.driver.back()



    def chrome(self):
        sleep(5)
        accept = self.driver.find_element_by_id('com.android.chrome:id/terms_accept')
        accept.click()


        try:
                next = self.driver.find_element_by_id('com.android.chrome:id/next_button')
                next.is_displayed()
            
                next.click()
        except:
                pass

    

    def sign_in(self):
        try:
            techlead = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.chrome:id/account_name") and contains(@text, "techleadqa@gmail.com")]')
            techlead.is_displayed()
            
            continue_button = self.driver.find_element_by_id('com.android.chrome:id/positive_button')
            continue_button.click()

            self.driver.swipe(367, 367, 380, 190, 400)
            
            ok = self.driver.find_element_by_id('com.android.chrome:id/positive_button')
            ok.click()

            print 'Success!: Sign in to Chrome.'

            self.web_page()
        except:
            add_account = self.driver.find_element_by_id('com.android.chrome:id/account_image')
            add_account.click()

            sleep(5)

            self.driver.tap([(310,545)], 500)

            # Sign in with a new account ...

        
        
    
    def testcase_chrome(self):
        self.chrome()
        self.sign_in()

   
        

    def tearDown(self):
        self.driver.quit()

    



        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))
     
    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1362", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1362", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit
     
