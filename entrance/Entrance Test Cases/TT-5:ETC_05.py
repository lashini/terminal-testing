# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.phone'
        desired_caps['appActivity'] = '.EmergencyDialer'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)
        

    def emergency_dial(self):
        emergNumber = self.driver.find_element_by_id('com.android.phone:id/digits')
        emergNumber.send_keys("194")

        dial = self.driver.find_element_by_id('com.android.phone:id/floating_action_button')
        dial.click()

        ok = self.driver.find_element_by_id('android:id/button1')
        ok.click()
        print 'SUCCESS! Emergency Dialer: Incorrect emergency number.'


    def emergency_dial2(self):
        emergNum = self.driver.find_element_by_id('com.android.phone:id/digits')
        emergNum.send_keys("911")

        dial = self.driver.find_element_by_id('com.android.phone:id/floating_action_button')
        dial.click()

        #returncall = self.driver.find_element_by_xpath('//android.widget.Button[contains(@text, "Return to call")]')
        #returncall.click()

        sleep(10)
        cancelcall = self.driver.find_element_by_id('com.android.dialer:id/floating_end_call_action_butto')
        cancelcall.click()
        print 'SUCCESS! Emergency Dialer: Correct emergency number-911 .'

        self.driver.back()

    def testcase_emergencyDial(self):
        self.emergency_dial()
        self.emergency_dial2()  

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    print result
   
    #pass -->       <unittest.runner.TextTestResult run=1 errors=0 failures=0>
    #fail -->       <unittest.runner.TextTestResult run=1 errors=1 failures=0>

    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("165", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
            print 'PASSED !'
        else:
            tls.reportTCResult("165", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)
            print 'FAILED !'
    except:
        exit
  


