# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def about_phone(self):
        self.driver.swipe(362, 1270, 362, 170, 400)
        self.driver.swipe(362, 1270, 362, 170, 400)
        self.driver.swipe(362, 1270, 362, 170, 400)
       

        about_phone = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.settings:id/title") and contains(@text, "About phone")]')
        about_phone.click()
        print 'SUCCESS!: View About Phone.'

        system_updates = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "System updates")]')
        system_updates.click()
        self.driver.implicitly_wait(5000)

        check_updates = self.driver.find_element_by_id('com.adups.fota:id/bt_check')
        check_updates.click()
        self.driver.implicitly_wait(5000)

        print 'SUCCESS!: Check system updates.'

        actions = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.adups.fota:id/btn_pop") and contains(@class, "android.widget.RelativeLayout")]')
        actions.click()

        exit = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Exit")]')
        exit.click()
    
        self.driver.back()
        sleep(3)



    def testcase_aboutPhone(self):
        self.about_phone()


    def tearDown(self):
        self.driver.quit()


        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1483", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1483", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit
