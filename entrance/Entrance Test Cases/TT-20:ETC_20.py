# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'android'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.google.android.gm'
        desired_caps['appActivity'] = 'com.google.android.gm.ConversationListActivityGmail'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def is_connected(self):

        x = True
        countif = 0
        countelif = 0
        while x is True and countif < 5:
            try:

                if (self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Primary")]')). is_displayed() is True:
                    print 'SUCCESS!: Checking mails'
                    self.driver.swipe(384, 530, 384, 1123, 300)
                    sleep(3)
                    self.inbox()
                    x = False

                return x
            except:
                try:
                    if (self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Account not synced")]')). is_displayed() is True and countelif < 2:
                        print 'ERROR!: Connection lost.Trying to connect again'
                        countelif = countelif+1
                    else:
                        print 'ERROR!: Connection Error'
                        x = False 
                    
                except:
                    print 'unknown'
            sleep(2)
            countif = countif + 1
        #pass

        self.driver.back()



    def check_gmail(self):
        try:
            x = self.driver.is_app_installed('com.google.android.gm')
            if x is True:
                print 'Gmail is already installed'
            else:
                print 'gmail is not installed'
        except:
            print 'gmail is not installed'



    def sign_in(self):
        welcome = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.google.android.gm:id/welcome_tour_got_it") and contains(@text, "Got it")]')
        welcome.click()
    

        #time wait....

        sleep(20)
        gmail = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.google.android.gm:id/action_done") and contains(@text, "Take me to Gmail")]')
        gmail.click()

        sleep(20)
        self.is_connected()



    def inbox(self):
        
        self.driver.tap([(411,401)], 500)
        sleep(5)
        
        self.driver.tap([(53,550)], 500)
        self.driver.tap([(95,557)], 500)
        sleep(5)
        print 'SUCCESS!: View received e-mail.'

        back = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageButton") and contains(@content-desc, "Navigate up")]')
        back.click()
        self.compose()



    def compose(self):
        compose = self.driver.find_element_by_xpath('//*[contains(@content-desc,"Compose") and contains(@resource-id, "com.google.android.gm:id/compose_button")]')
        compose.click()
        self.driver.implicitly_wait(5000)


        to = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.MultiAutoCompleteTextView") and contains(@resource-id, "com.google.android.gm:id/to")]')
        to.send_keys("himali@techleadintl.com")
        self.driver.press_keycode(66)    #Enter

        subject = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.EditText") and contains(@resource-id, "com.google.android.gm:id/subject")]')
        subject.send_keys("This is a Test E-mail.")

        body = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@resource-id, "com.google.android.gm:id/composearea_tap_trap_bottom")]')
        #self.driver.tap([(70,560)], 500)
        body.send_keys("Test.")

        print 'SUCCESS!: Compose e-mail.'

        send = self.driver.find_element_by_id('com.google.android.gm:id/send')
        send.click()
        sleep(5)
        print 'SUCCESS!: Send e-mail.'

        self.sent()


    def sent(self):

        drawer = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageButton") and contains(@content-desc, "Open navigation drawer")]')
        drawer.click()

        sent = self.driver.find_element_by_xpath('//*[contains(@text,"Sent") and contains(@resource-id, "com.google.android.gm:id/name")]')
        sent.click()

        sleep(5)

        
        try:
            sent_mail = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@content-desc, " me, This is a Test E-mail., Test.,  at")]')
            sent_mail.is_displayed()
            
            print 'SUCCESS!: Mail is sent.'
            
            actions = TouchAction(self.driver)
            actions.long_press(sent_mail)
            actions.perform()
        
            delete = self.driver.find_element_by_id('com.google.android.gm:id/delete')
            delete.click() 
            print 'SUCCESS!: Delete sent mail.'
            
            '''
            try:
                delete_mail = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@content-desc, " me, This is a Test E-mail., Test.,  at")]')
                delete_mail.is_displayed()
            
                print 'ERROR!: Mail is not deleted.'
            except:
                print 'SUCCESS!: Delete sent mail.'
            '''
            
        except:
            print 'ERROR!: Mail is not sent.'

        #self.switch_off_wifi()

        

    '''
    def switch_off_wifi(self):
        self.driver.swipe(340, 1, 340, 800, 2000)

        notification = self.driver.find_element_by_id('com.android.systemui:id/header')
        notification.click()

        wifi = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@content-desc, "WLAN signal full..")]')
        wifi.click()

        print 'SUCCESS!: Wifi is switched off.'


        mobiledata = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@text, "Mobile data")]')
        mobiledata.click()
        print 'SUCCESS! Mobile data is switched off.'
    '''


    def testcase_gmail(self):
        self.check_gmail()
        self.sign_in()
      
     
        

    def tearDown(self):
        self.driver.quit()
 



        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1467", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1467", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit

