# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class EntranceTests(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.gallery3d'
        desired_caps['appActivity'] = '.app.GalleryActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def view_gallery(self):
        self.driver.tap([(366,666)], 500)
        sleep(3)
        print 'SUCCESS!: View gallery.'

    def view_images(self):
        self.driver.tap([(424,218)],500)
        sleep(3)

    def share_image(self):
        try:
            actionBar = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@resource-id, "android:id/action_bar")]')
            if actionBar.is_displayed():
                self.driver.find_element_by_xpat('//*[contains(@resource-id, "android:id/expand_activities_button") and contains(@class, "android.widget.FrameLayout")]').click()
                self.select_bluetooth()
            else:
                sleep(2)
                self.driver.tap([(380,560)],500)
                self.driver.find_element_by_xpath('//*[contains(@resource-id, "android:id/expand_activities_button") and contains(@class, "android.widget.FrameLayout")]').click()
                self.select_bluetooth()
        except:
            sleep(2)
            self.driver.tap([(380,560)],500)
            self.driver.find_element_by_xpath('//*[contains(@resource-id, "android:id/expand_activities_button") and contains(@class, "android.widget.FrameLayout")]').click()
            self.select_bluetooth()

    def select_bluetooth(self):
        shareByBluetoothIcon = self.driver.find_element_by_xpath('//*[contains(@index, "1") and contains(@resource-id, "android:id/list_item") and contains(@class, "android.widget.LinearLayout")]')
        shareByBluetoothIcon.click()
        print 'SUCCESS!: Select bluetooth.'
        sleep(5)
        try:
            if (self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.FrameLayout") and contains(@resource-id, "android:id/content") and contains(@index, "0")]')).is_displayed() :
                self.driver.find_element_by_xpath('//*[contains(@text, "Turn on") and contains(@class,"android.widget.Button") and contains(@resource-id, "android:id/button1") and contains(@index, "1")]').click()
                sleep(5)
                self.select_device()
        except:
            self.select_device()

    def select_device(self):

        x = True
        countif = 0
        countelif = 0
        while x is True and countif < 6:
            if countif<4:
                try:
                    bluetoothDevice = self.driver.find_element_by_xpath('//*[contains(@text,"techlead-Inspiron") and contains(@class, "android.widget.TextView")]')
                    bluetoothDevice.click()
                    print 'SUCCESS!: Select device.'
                    print 'SUCCESS!: Share image'
                    sleep(2)
                    x = False
                except:
                    sleep(2)
                    countif = countif + 1
                    print countif
            elif countif is 4 and countelif < 1:
                self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.ImageButton") and contains(@package, "com.android.settings") and contains(@content-desc, "More options")]').click()
                self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.TextView") and contains(@resourse-id, "android:id/title") and contains(@text, "Refresh")]').click()
                countif = 1
                countelif = 1
            else:
                print 'ERROR!: Cannot find the given bluetooth device'

            pass

        self.driver.back()


    def testcase_bluetooth(self):
        self.view_gallery()
        self.view_images()
        self.share_image() 

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1279", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1279", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit
