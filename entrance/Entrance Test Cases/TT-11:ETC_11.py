# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class EntranceTests(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        #org.codeaurora.snapcam/com.android.camera.CameraLauncher
        desired_caps['appPackage'] = 'org.codeaurora.snapcam'
        desired_caps['appActivity'] = 'com.android.camera.CameraLauncher'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)


    def check_first_using(self):
    	
        try:
            popUpScreen = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.FrameLayout") and contains(@resource-id, "android:id/content") and contains(@package, "org.codeaurora.snapcam")]')
    	    popUpScreen.is_displayed()
    	    self.cancel_adding_photo_location()
    	    self.select_option()
    	except:
    		self.select_option()

    def cancel_adding_photo_location(self):
        noThanksButton = self.driver.find_element_by_xpath('//*[contains(@text,"No thanks") and contains(@resource-id, "android:id/button2")]')
        noThanksButton.click()
        sleep(1)
        #self.driver.implicitly_wait(100)

    def select_option(self):
    	self.driver.find_element_by_id("org.codeaurora.snapcam:id/camera_switcher").click()
    	print 'SUCCESS!: Select option.'
    	self.select_camera()

    def select_camera(self):
    	print 'SUCCESS!: Select camera.'
        #self.driver.find_element_by_id("org.codeaurora.snapcam:id/camera_switcher").click()
     	self.driver.find_element_by_xpath('//*[contains(@content-desc,"Switch to photo") and contains(@class, "android.widget.ImageView")]').click() 	
        sleep(2)
        self.take_selfy()

    def take_selfy(self):
        self.driver.find_element_by_xpath('//*[contains(@resource-id, "org.codeaurora.snapcam:id/front_back_switcher") and contains(@class, "android.widget.ImageView") and contains(@index, "3")]').click()
        self.driver.find_element_by_xpath('//*[contains(@index, "8") and contains(@resource-id, "org.codeaurora.snapcam:id/shutter_button")]').click()
        self.driver.find_element_by_xpath('//*[contains(@resource-id, "org.codeaurora.snapcam:id/front_back_switcher") and contains(@class, "android.widget.ImageView") and contains(@index, "3")]').click()
        print 'SUCCESS!: Take selfie.'
        self.take_photo()

    def take_photo(self):
    	
        #self.driver.find_element_by_xpath('//*[contains(@index, "8") and contains(@resource-id, "org.codeaurora.snapcam:id/shutter_button")]').click()
        i = 1
        for i in range (1,6):
        	self.capture_scene()
        	sleep(4)
        	i = i + 1

        print 'SUCCESS!: Take photos.'

        self.select_video()


    def capture_scene(self):
        self.driver.find_element_by_xpath('//*[contains(@index, "8") and contains(@resource-id, "org.codeaurora.snapcam:id/shutter_button")]').click()
       #self.selectVideo()

    # def viewImages(self):
    #     sleep(5)
    #     self.driver.find_element_by_xpath('//*[contains(@index, "7") and contains(@resource-id, "org.codeaurora.snapcam:id/preview_thumb")]').click()
    #     sleep(3)
    #     self.driver.back()

    def select_video(self):
    	self.driver.find_element_by_id("org.codeaurora.snapcam:id/camera_switcher").click()
      	self.driver.find_element_by_xpath('//*[contains(@content-desc,"Switch to video") and contains(@class, "android.widget.ImageView")]').click() 	 
        self.record_video()

    def record_video(self):
    	self.driver.find_element_by_xpath('//*[contains(@index,"8") and contains(@resource-id, "org.codeaurora.snapcam:id/shutter_button")]').click()
    	sleep(5)
        print 'SUCCESS!: Record video.'
        self.driver.find_element_by_xpath('//*[contains(@resource-id, "org.codeaurora.snapcam:id/shutter_button")]').click()
        self.view_images_and_videos()
        
    def view_images_and_videos(self):
    	self.driver.find_element_by_xpath('//*[contains(@index, "7") and contains(@resource-id, "org.codeaurora.snapcam:id/preview_thumb")]').click()
        sleep(5)
        self.driver.tap([(353,644)],450)
        sleep(5)
        print 'SUCCESS!: Play video.'
        self.driver.swipe(642, 620, 126, 600, 400)
        sleep(8)
        print 'SUCCESS!: View image.'
        self.driver.back()

    def testcase_camera(self):
    	self.check_first_using()

    def tearDown(self):
    	self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("864", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("864", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit

    
    
