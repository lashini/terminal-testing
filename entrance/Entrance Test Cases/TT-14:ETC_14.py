# -*- coding: utf-8 -*
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.systemui'
        desired_caps['appActivity'] = ' '
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def home(self):
        
        '''
            # Long press 'Home' screen

                self.driver.press_keycode(82)     
            
                settings = self.driver.find_element_by_id('com.android.launcher3:id/settings_button')
                settings.click()
      
        '''


        self.driver.tap([(345,1200)], 500)

        #self.driver.swipe(470, 800, 50, 800, 400)


        #fileMng = self.driver.find_element_by_xpath('//*[contains(@content-desc,"File Manager") and contains(@text, "File Manager")]')
        #fileMng.click()
        

        music_player = self.driver.find_element_by_xpath('//*[contains(@content-desc,"Music") and contains(@text, "Music")]')
        music_player.click()
        sleep(3)

        print 'SUCCESS!: Open app from HOME'


    def player(self):

        albums = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.music:id/albumtab") and contains(@text, "Albums")]')
        albums.click()

        recordings = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.music:id/line1") and contains(@text, "Audio recordings")]')
        recordings.click()
        sleep(3)
        
        play =  self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Your recordings")]')
        play.click()

        print 'SUCCESS!: Play sountrack.'

        self.driver.back()
        

    def testcase_musicPlayer(self):
        self.home()
        self.player()
        

    def tearDown(self):
        self.driver.quit()




        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1169", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1169", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit

