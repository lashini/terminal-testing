# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.calculator2'
        desired_caps['appActivity'] = '.Calculator'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)
        

    def testcase_calculator(self):
        
        print 'SUCCESS! Built-in app, Calculator open.'

        num1 = self.driver.find_element_by_id('com.android.calculator2:id/digit_7').click()
        mul = self.driver.find_element_by_id('com.android.calculator2:id/op_mul').click()
        num2 = self.driver.find_element_by_id('com.android.calculator2:id/digit_1').click()
        num2 = self.driver.find_element_by_id('com.android.calculator2:id/digit_0').click()

        eq = self.driver.find_element_by_id('com.android.calculator2:id/eq').click()


        self.driver.back()
        #self.driver.swipe(470, 800, 50, 800, 400)
        sleep(5)

        self.driver.tap([(345,1200)], 500)

        
        
    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1494", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1494", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit