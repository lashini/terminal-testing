# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.systemui'
        desired_caps['appActivity'] = ' '
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)


    def data_connection(self):
        self.driver.orientation = "PORTRAIT"
        self.driver.swipe(340, 1, 340, 800, 2000)

        notification = self.driver.find_element_by_id('com.android.systemui:id/header')
        notification.click()

        try:
                wifi = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "WLAN")]')
                wifi.is_displayed()
                print 'Wifi is switched off'

                mobiledata = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@text, "Mobile data")]')
                mobiledata.click()
                print 'SUCCESS! Switch on Mobile data'
                sleep(5)

        except:
                print 'Wifi is switched on'

                wifi_off = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageView") and contains(@index, "0")]')
                wifi_off.click()
                print 'SUCCESS! Switch off Wifi'

                mobiledata = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@text, "Mobile data")]')
                mobiledata.click()
                print 'SUCCESS! Switch on Mobile data'
                sleep(5)
       
    
    def testcase_dataAndWifi(self):
        self.data_connection()
    

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("239", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("239", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit
