# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    

    def date_time(self):

        self.driver.swipe(362, 1270, 362, 170, 400)
        self.driver.swipe(362, 1270, 362, 170, 400)
        self.driver.swipe(362, 1270, 362, 170, 400)

        sleep(3)
        date_time = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.settings:id/title") and contains(@text, "Date & time")]')
        date_time.click()
       
        automatic_toggle = self.driver.find_element_by_id('android:id/switchWidget')
        automatic_toggle.click()

        print 'SUCCESS!: Switch off automatic date & time.'

        date_set = self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/title") and contains(@text, "Set date")]')
        date_set.click()

        sleep(3)

        self.driver.swipe(368, 638, 373, 1044, 400)

        date = self.driver.find_element_by_xpath('//*[contains(@class,"android.view.View") and contains(@index, "14")]')
        date.click()

        ok = self.driver.find_element_by_id('android:id/button1')
        ok.click()

        print 'SUCCESS!: Date changed.'

        
        time = self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/title") and contains(@text, "Set time")]')
        time.click()

        timeHR = self.driver.find_element_by_id('android:id/hours')
        timeHR.click()
        hr = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "2")]')
        hr.click()
        timeMIN = self.driver.find_element_by_id('android:id/minutes')
        timeMIN.click()
        min = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "9")]')
        min.click()
        PM = self.driver.find_element_by_id('android:id/pm_label')
        PM.click()

        ok = self.driver.find_element_by_id('android:id/button1')
        ok.click()

        print 'SUCCESS!: Time changed.'

        automatic_toggle = self.driver.find_element_by_id('android:id/switchWidget')
        automatic_toggle.click()

        print 'SUCCESS!: Switch on automatic date & time again.'

        self.driver.back()


    def wait(self):
        self.driver.implicitly_wait(150)


    def testcase_dateAndTime(self):
        self.date_time()


    def tearDown(self):
        self.driver.quit()


        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1444", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1444", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit