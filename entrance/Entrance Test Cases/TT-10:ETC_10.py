# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def contact_view(self):
                self.driver.find_element_by_xpath('//*[contains(@text,"Contacts") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
                self.driver.find_element_by_xpath('//*[contains(@text,"All") and contains(@class, "android.widget.RadioButton") and contains(@resource-id, "com.camtalk.start:id/friend_rb_all")]').click()
                print 'SUCCESS: Contacts view.'

                sleep(3)
                x = False
                i = 0
                while x is False:
                    for i in range(0,9):
                        try:
                            self.driver.find_element_by_xpath('//*[contains(@index, "1") and contains(@class, "android.widget.RelativeLayout") and contains(@package, "com.camtalk.start")]').is_displayed()
                            x = True
                            i = 10
                            self.make_call()                           
                        except:
                            self.driver.implicitly_wait(300)
                            i = i + 1

                

    def make_call(self):
        #self.driver.tap([(550,780)], 500)  

        search = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.EditText") and contains(@resource-id, "com.camtalk.start:id/contacts_et_search")]')
        search.send_keys("Tester 1")
        self.driver.press_keycode(66)    #Enter
        sleep(6)

        receiver = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Tester 1")]')
        receiver.click()

        call= self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageView") and contains(@resource-id, "com.camtalk.start:id/detail_call_phone")]')
        call.click()

        sim_call = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "SIM Call")]')
        sim_call.click()
        sleep(3)

        cancel_call = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.dialer:id/floating_end_call_action_button")]')
        cancel_call.click()
        print 'SUCCESS: Make a call.'
        
        back = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageView") and contains(@resource-id, "com.camtalk.start:id/back_drawable")]')
        back.click()
        #self.driver.tap([(56,117)], 500)  


    def add_contact(self):
        
        print 'qaz'
        me = self.driver.find_element_by_xpath('//*[contains(@text,"Me") and contains(@class, "android.widget.TextView")]')
        me.click()

        self.driver.swipe(387, 1097, 387, 260, 400)

        confirm = self.driver.find_element_by_xpath('//*[contains(@text,"Confirm") and contains(@class, "android.widget.Button") and contains(@resource-id, "android:id/button1")]')
        confirm.click()

        country = self.driver.find_element_by_id('com.camtalk.start:id/activity_login_country_name')
        country.click()

        cambodia = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@text, "Cambodia")]')
        cambodia.click()

        phone_num = self.driver.find_element_by_xpath('//android.widget.EditText[contains(@resource-id, "com.camtalk.start:id/activity_login_home_phone_num")]')
        phone_num.send_keys("388009043")

        password = self.driver.find_element_by_xpath('//android.widget.EditText[contains(@resource-id, "com.camtalk.start:id/activity_login_by_pw_pw")]')
        password.send_keys("123456")

        login = self.driver.find_element_by_xpath('//android.widget.TextView[contains(@resource-id, "com.camtalk.start:id/activity_login_home_next_step")]')
        login.click()
        sleep(10)

        try:
            update_window = self.driver.find_element_by_xpath('//android.widget.LinearLayout[contains(@resource-id, "android:id/title_template")]')
            if update_window.is_displayed():
                sync_cancel = self.driver.find_element_by_id('android:id/button2')
                sync_cancel.click()

                update = self.driver.find_element_by_id('android:id/button2')
                update.click()
                print 'SUCCES: Log in.'
            else:
                print ' '
        except:
                print 'SUCCES: Log in.'




        add = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageView") and contains(@resource-id, "com.camtalk.start:id/right_drawable")]')
        add.click()

        add_contact = self.driver.find_element_by_xpath('//*[contains(@text,"Add phone contacts") and contains(@resource-id, "com.camtalk.start:id/title")]')
        add_contact.click()

        first_name = self.driver.find_element_by_xpath('//android.widget.EditText[contains(@resource-id, "com.camtalk.start:id/activity_add_contact_first_name")]')
        first_name.send_keys("Tester 2")

        number = self.driver.find_element_by_xpath('//android.widget.EditText[contains(@resource-id, "com.camtalk.start:id/et_info_input")]')
        number.send_keys("0712014105")

        complete = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Complete")]')
        complete.click()
        print 'SUCCESS: Add contact.'

        self.driver.back()


    def edit_contact(self):
        sleep(5)
        select_contact = self.driver.find_element_by_xpath('//*[contains(@text,"Tester 2") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_")]')

        action = TouchAction(self.driver)
        action.tap(select_contact).perform()

        options = self.driver.find_element_by_id('com.camtalk.start:id/right_drawable')
        options.click()

        edit_selection = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Edit")]')
        edit_selection.click()

        edit = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.EditText") and contains(@index, "1")]')
        edit.clear()
        edit.send_keys("Tester 2 (new number)")

        confirm = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Confirm")]')
        confirm.click()

        print 'SUCCESS: Edit contact.'
        sleep(3)
        
        self.driver.back()


    def delete_contact(self):
        select_contact = self.driver.find_element_by_xpath('//*[contains(@text,"Tester 2") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_contact_name")]')

        action = TouchAction(self.driver)
        action.tap(select_contact).perform()

        options_open = self.driver.find_element_by_id('com.camtalk.start:id/right_drawable')
        options_open.click()

        delete_selection = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Delete")]')
        delete_selection.click()

        delete_confirm = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.TextView") and contains(@text, "Confirm")]')
        delete_confirm.click()
        print 'SUCCESS: Delete contact.'

        self.driver.back()


    def testcase_contacts(self):
        self.contact_view() 
        self.make_call()
        self.add_contact()
        self.edit_contact()
        self.delete_contact()
        

    def tearDown(self):
        self.driver.quit()

        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))
    '''
    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("272", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("272", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit
    '''
