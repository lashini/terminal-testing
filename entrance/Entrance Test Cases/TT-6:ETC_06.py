# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.mms'
        desired_caps['appActivity'] = 'com.android.mms.ui.ConversationList'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def send_sms(self):
        addmsg = self.driver.find_element_by_xpath('//android.widget.ImageButton[contains(@index, "0")]')
        addmsg.click()

        editreceiver = self.driver.find_element_by_id('com.android.mms:id/recipients_editor')
        editreceiver.send_keys("0713414713")

        message = self.driver.find_element_by_id('com.android.mms:id/embedded_text_editor_btnstyle')
        message.send_keys("Test SMS")

        sendsms = self.driver.find_element_by_id('com.android.mms:id/first_send_button_sms_view')
        sendsms.click()

        print 'SUCCESS! Send SMS'

        back = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageButton") and contains(@content-desc, "Navigate up")]')
        back.click()
    

    

    def delete_sms(self):

        sent_sms = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.mms:id/subject") and contains(@text, "Test SMS")]')
        
        actions = TouchAction(self.driver)
        actions.long_press(sent_sms)
        actions.perform()
    
        delete = self.driver.find_element_by_id('com.android.mms:id/delete')
        delete.click() 

        confirm_delete = self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/button1") and contains(@class, "android.widget.Button")]') 
        confirm_delete.click()
        
        print 'SUCCESS!: Delete SMS.'





    def testcase_sendSMS(self):
        self.send_sms() 
        self.delete_sms()
        #self.read_sms()
        


    def tearDown(self):
        self.driver.quit()





        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("212", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("212", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit

