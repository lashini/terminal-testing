# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.deskclock'
        desired_caps['appActivity'] = '.DeskClock'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        #print 'my url',url
        self.driver = webdriver.Remote(url, desired_caps)


    def date_select(self):

    # Alarm should be set to Mon & Wed

        mon = self.driver.find_element_by_xpath('//*[contains(@text,"Mon")]')
        if  mon.is_enabled():
              pass
        else:
            mon.click()

        tue = self.driver.find_element_by_xpath('//*[contains(@text,"Tue")]')
        if  tue.is_enabled():
              tue.click()
        else:
                pass

        wed = self.driver.find_element_by_xpath('//*[contains(@text,"Wed")]')
        if  wed.is_enabled():
              pass
        else:
                wed.click()

        thu = self.driver.find_element_by_xpath('//*[contains(@text,"Thu")]')
        if  thu.is_enabled():
              thu.click()
        else:
                pass

        fri = self.driver.find_element_by_xpath('//*[contains(@text,"Fri")]')
        if  fri.is_enabled():
              fri.click()
        else:
                pass

        sat = self.driver.find_element_by_xpath('//*[contains(@text,"Sat")]')
        if  sat.is_enabled():
              sat.click()
        else:
            pass

        sun = self.driver.find_element_by_xpath('//*[contains(@text,"Sun")]')
        if  sun.is_enabled():
                sun.click() 
        else:
            pass



    def default_alarm(self):
        defaultAlarms = self.driver.find_element_by_accessibility_id('Alarm')
        defaultAlarms.click()

        setAlarm = self.driver.find_element_by_id('com.android.deskclock:id/onoff')
        setAlarm.click()

        print 'SUCCESS!: Switch ON default alarm.'




    def set_alarm(self):
        time = self.driver.find_element_by_id('com.android.deskclock:id/digital_clock')
        time.click()

        timeHR = self.driver.find_element_by_id('android:id/hours')
        timeHR.click()
        hr = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "2")]')
        hr.click()
        timeMIN = self.driver.find_element_by_id('android:id/minutes')
        timeMIN.click()
        min = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "9")]')
        min.click()
        PM = self.driver.find_element_by_id('android:id/pm_label')
        PM.click()

        okay = self.driver.find_element_by_id('android:id/button1')
        okay.click()

        print 'SUCCESS!: Edit time.'


        #editAlarm = self.driver.find_element_by_id('com.android.deskclock:id/arrow')
        #editAlarm.click()


        increaseVolume = self.driver.find_element_by_id('com.android.deskclock:id/increasing_volume_onoff')
        increaseVolume.click()
        vibrate = self.driver.find_element_by_id('com.android.deskclock:id/vibrate_onoff')
        vibrate.click()
        alarmtype = self.driver.find_element_by_id('com.android.deskclock:id/choose_ringtone')
        alarmtype.click()


        selectringtone = self.driver.find_element_by_id('android:id/text1')
        selectringtone.click()
        ringtone = self.driver.find_element_by_xpath('//android.widget.CheckedTextView[contains(@index, "5")]')
        ringtone.click()

        ok = self.driver.find_element_by_id('android:id/button1')
        ok.click()


        #repeat = self.driver.find_element_by_id('com.android.deskclock:id/repeat_onoff')
        #repeat.click()

        self.date_select() 

    
        label = self.driver.find_element_by_id('com.android.deskclock:id/edit_label')
        label.click()
        editlabel1_a = self.driver.find_element_by_id('com.android.deskclock:id/labelBox')
        editlabel1_a.send_keys("Test Alarm")
        
        labelOK = self.driver.find_element_by_id('com.android.deskclock:id/setButton')
        labelOK.click()
        
        print 'SUCCESS!: Set alarm.'


        #alarm = self.driver.find_element_by_accessibility_id('Alarm')
        #alarm.click()

        #editlabel1_b = self.driver.find_element_by_id('com.android.deskclock:id/label')
        #if editlabel1_b.is_displayed:
            #pass
            #print 'YES!'
        #else:
           # print 'ERROR! No Alarm Title!'



        #editlabel2_a = self.driver.find_element_by_id('com.android.deskclock:id/nextAlarm')
        #editlabel2_a.clear()

        #labelOK = self.driver.find_element_by_id('com.android.deskclock:id/setButton')
        #labelOK.click()
        
       

        home = self.driver.find_element_by_accessibility_id('Clock')
        home.click()

        try:
            nextalarm = self.driver.find_element_by_id('com.android.deskclock:id/nextAlarm')
            nextalarm.is_displayed()
            print 'Success!: Alarm icon is displayed'
        except:
            print 'Error Occured: No alarm icon is displayed'


    

    def add_default_alarm(self):
        defaultAlarms = self.driver.find_element_by_accessibility_id('Alarm')
        defaultAlarms.click()

        add = self.driver.find_element_by_id('com.android.deskclock:id/fab')
        add.click()

        timeHR = self.driver.find_element_by_id('android:id/hours')
        timeHR.click()
        hr = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "11")]')
        hr.click()
        timeMIN = self.driver.find_element_by_id('android:id/minutes')
        timeMIN.click()
        min = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.RadialTimePickerView") and contains(@index, "0")]')
        min.click()
        PM = self.driver.find_element_by_id('android:id/pm_label')
        PM.click()

        okay = self.driver.find_element_by_id('android:id/button1')
        okay.click()

        print 'SUCCESS!: Add new alarm.'

        sleep(3)


    def delete_alarm(self):
        delete = self.driver.find_element_by_id('com.android.deskclock:id/delete')
        delete.click()

        print 'SUCCESS!: Delete new alarm.'
        sleep(3)


    def testcase_alarm(self):
        self.default_alarm()  
        self.set_alarm()
        self.add_default_alarm()
        self.delete_alarm()
        



    def tearDown(self):
        self.driver.quit()



   
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1478", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1478", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit