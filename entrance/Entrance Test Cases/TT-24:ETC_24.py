# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class EntranceTests(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.vending'
        desired_caps['appActivity'] = '.AssetBrowserActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
        self.driver.orientation = 'PORTRAIT'
   
    def wait_for_app(self):
        sleep(5)
        self.check_installed_apps()

    def check_installed_apps(self):
        try:
            x = self.driver.is_app_installed('lk.express.sltb')
            if x is True:
                print 'SLTB Express is already installed'
                print 'Uninstalling SLTB Express'
                self.driver.remove_app('lk.express.sltb')
                print 'successfully Uninstalled SLTB Express'
                print 'Reinstalling SLTB Express'
                self.check_connection()
            else:
                self.check_connection()
        except:
            print 'yes iiii'
            self.check_connection()

    def check_connection(self):
        x = True
        countif = 0
        countelif = 0
        while x is True and countif < 5:
            if countif<4:
                try:
                    searchBar = self.driver.find_element_by_id("com.android.vending:id/play_search_container")
                    searchBar.click()
                    print 'SUCCESS!: Searching for the app.'
                    sleep(2)
                    self.install_app()
                    x = False
                    return False
                except:
                    sleep(2)
                    countif = countif + 1
            elif countif is 4 and countelif < 1:
                print 'ERROR!: Trying to connect again'
                countif = 1
                countelif = 1
            else:
                print 'ERROR!: Connection Error'
                countif =countif +1
            #pass

        self.driver.back()


    def install_app(self):
        #self.driver.find_element_by_id("com.android.vending:id/play_search_container").click()
        searchText = self.driver.find_element_by_id("com.android.vending:id/search_box_text_input")
        searchText.send_keys("SLTB Express")
        sleep(2)
        self.driver.keyevent(66)
        sleep(5)

        #select app to be installed
        self.driver.find_element_by_xpath('//*[contains(@text,"SLTB EXPRESS") and contains(@resource-id, "com.android.vending:id/li_title")]').click()
        sleep(10)

        #install app
        self.driver.find_element_by_xpath('//*[contains(@text,"INSTALL") and contains(@class, "android.widget.Button") and contains(@package, "com.android.vending")]').click()
        sleep(3)

        x = False

        while x is False:
            try:
                x = self.driver.find_element_by_xpath('//*[contains(@text,"OPEN") and contains(@class, "android.widget.Button") and contains(@package, "com.android.vending")]').is_displayed()
                if x is True:
                    self.driver.find_element_by_xpath('//*[contains(@text,"OPEN") and contains(@class, "android.widget.Button") and contains(@package, "com.android.vending")]').click()
                    sleep(5)
                else:
                    print 'trying again'
                return x
            except:
                self.driver.implicitly_wait(300)

       


    def removing_app(self):
        self.driver.remove_app('lk.express.sltb')
        print 'successfully removed'
        self.driver.back()

    def testcase_playStore(self):
        self.wait_for_app()  
        self.removing_app() 

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1489", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1489", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit