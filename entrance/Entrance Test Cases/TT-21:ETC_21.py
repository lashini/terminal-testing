# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep

PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)



    def wifi_select(self):

        wifi = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.settings:id/title") and contains(@text, "WiFi")]')
        wifi.click()

        #wifi_toggle = self.driver.find_element_by_id('com.android.settings:id/switch_widget')

        try:
                wifi_toggle = self.driver.find_element_by_id('com.android.settings:id/switch_widget')
                wifi_toggle.is_enabled()
            
                print 'Wifi is already switched on.'
                select_connection = self.driver.find_element_by_xpath('//*[contains(@text,"TECHLEAD")]')
                select_connection.click()
                forget_connection = self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/button3") and contains(@text, "Forget")]')
                forget_connection.click()
        except:
                print 'Wifi is currently switched off.'
                wifi_toggle.click()
                print 'SUCCESS!: Wifi switch ON'
                sleep(10)
                select_connection = self.driver.find_element_by_xpath('//*[contains(@text,"TECHLEAD")]')
                select_connection.click()
                forget_connection = self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/button3") and contains(@text, "Forget")]')
                forget_connection.click()


        select_connection = self.driver.find_element_by_xpath('//*[contains(@text,"TECHLEAD")]')
        select_connection.click()


        print 'SUCCESS!: Select wifi.'



    def set_connection(self):

        show_password = self.driver.find_element_by_id('com.android.settings:id/show_password')
        show_password.click()

        password = self.driver.find_element_by_xpath('//android.widget.EditText[contains(@resource-id, "com.android.settings:id/password")]')
        password.send_keys("mangotl1")

        connect = self.driver.find_element_by_id('android:id/button1')
        connect.click()

        sleep(10)

        print 'SUCCESS!: Set connection.'

        self.driver.back()



    def wait(self):
        self.driver.implicitly_wait(150)


    def testcase_settings_wifi(self):
        self.wifi_select() 
        self.set_connection()
        self.wait()

        

    def tearDown(self):
        self.driver.quit()


        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1473", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("1473", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit