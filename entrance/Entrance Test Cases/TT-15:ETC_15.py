# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class EntranceTests(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.gallery3d'
        desired_caps['appActivity'] = '.app.GalleryActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def view_gallery(self):
        self.driver.tap([(366,666)], 500)
        sleep(3)
        print 'SUCCESS!: View gallery.'

    def view_images(self):
        self.driver.tap([(424,218)],500)
        sleep(3)

        self.driver.back()

    def view_video(self):
        self.driver.tap([(209,263)],450)
        print 'SUCCESS!: View video.'

    def play_video(self):

        sleep(2)
        self.driver.tap([(369,628)],450)
        sleep(6)
        print 'SUCCESS!: Play video.'

    def testcase_gallery(self):
        self.view_gallery()
        self.view_images()
        self.view_video()
        self.play_video()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1200", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1200", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit
