# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.cyanogenmod.filemanager'
        desired_caps['appActivity'] = '.activities.NavigationActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)


    def fileMng(self):
        #openDrawer = self.driver.find_element_by_xpath('//*[contains(@content-desc,"Open navigation drawer") and contains(@index, "0")]')
        #openDrawer.click()
        #storage = self.driver.find_element_by_xpath('//*[contains(@text,"Internal storage") and contains(@content-desc, "com.cyanogenmod.filemanager:id/bookmarks_item_name")]')
        #storage.click()
        #sleep(3)

        imageFolder = self.driver.find_element_by_xpath('//*[contains(@text,"Image") and contains(@resource-id, "com.cyanogenmod.filemanager:id/navigation_view_item_name")]')
        imageFolder.click()

        searchOptions = self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.ImageButton") and contains(@resource-id, "com.cyanogenmod.filemanager:id/ab_button1")]')
        searchOptions.click()

        sort = self.driver.find_element_by_xpath('//*[contains(@text,"Sort results mode") and contains(@resource-id, "android:id/title")]')
        sort.click()

        byName = self.driver.find_element_by_xpath('//*[contains(@text,"By name") and contains(@resource-id, "android:id/text1")]')
        byName.click()
        print 'SUCCESS!: Sort by name.'

        back = self.driver.find_element_by_xpath('//*[contains(@content-desc,"Navigate up") and contains(@class, "android.widget.ImageButton")]')
        back.click()
        back = self.driver.find_element_by_xpath('//*[contains(@content-desc,"Navigate up") and contains(@class, "android.widget.ImageButton")]')
        back.click()

        imageFolder = self.driver.find_element_by_xpath('//*[contains(@text,"Image") and contains(@resource-id, "com.cyanogenmod.filemanager:id/navigation_view_item_name")]')
        imageFolder.click()
        
        selectImage = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.cyanogenmod.filemanager:id/search_item") and contains(@class, "android.widget.RelativeLayout")]')
        selectImage.click()
        sleep(3)

        self.driver.back()
        print 'SUCCESS!: Image view.'


    def testcase_fileMng(self):
        self.fileMng()


    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("263", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Himali', platformid=1)
        else:
            tls.reportTCResult("263", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Himali', platformid=1)

    except:
        exit

    
    