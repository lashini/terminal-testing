# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.dialer'
        desired_caps['appActivity'] = 'com.android.dialer.DialtactsActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_app(self):
        sleep(5)
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/floating_action_button") and contains(@class, "android.widget.ImageButton")]').click()
        
        print 'SUCCESS!: Successfully enter the SYSTEM dialing interface'
        sleep(3)
        self.dial_number()

    def dial_number(self):
        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/digits") and contains(@class, "android.widget.EditText")]')
        textBox.clear()
        textBox.send_keys("12345")
        self.delete_numbers()

    def delete_numbers(self):
        i=0
        j = 6
        for i in range (0,3):
            j = j - 1
            l = str(j)
            self.driver.find_element_by_id("com.android.dialer:id/deleteButton").click()
            print 'SUCCESS!: Deleted digit '+ l +' from last'
            i = i + 1

        self.check_number()

    def check_number(self):
        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/digits") and contains(@class, "android.widget.EditText")]')

        if (textBox.text == '12'):
            print 'SUCCESS!: Each digital input can be accurately'
        else:
            print 'ERROR!: '
            raise

        self.driver.back()

    def testcase_smartDialEnterNumbers(self):
        self.open_app()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("3464", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("3464", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


    