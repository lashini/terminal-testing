# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.dialer'
        desired_caps['appActivity'] = 'com.android.dialer.DialtactsActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_app(self):
        sleep(5)
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/floating_action_button") and contains(@class, "android.widget.ImageButton")]').click()
        
        print 'SUCCESS!: Successfully enter the SYSTEM dialing interface'
        sleep(3)
        self.dial_number()

    def dial_number(self):
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/zero")]').click()
        print 'SUCCESS!: Number 0 entered'
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/one")]').click()
        print 'SUCCESS!: Number 1 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/two")]').click()
        print 'SUCCESS!: Number 2 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/three")]').click()
        print 'SUCCESS!: Number 3 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/four")]').click()
        print 'SUCCESS!: Number 4 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/five")]').click()
        print 'SUCCESS!: Number 5 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/six")]').click()
        print 'SUCCESS!: Number 6 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/seven")]').click()
        print 'SUCCESS!: Number 7 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/eight")]').click()
        print 'SUCCESS!: Number 8 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/nine")]').click()
        print 'SUCCESS!: Number 9 entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/star")]').click()
        print 'SUCCESS!: Symbol * entered '
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/pound")]').click()
        print 'SUCCESS!: Symbol # entered '
        
        
        pauseSymbol = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/star")]')
        actions = TouchAction(self.driver)
        actions.long_press(pauseSymbol)
        actions.perform()
        print 'SUCCESS!: Symbol , entered '
        #self.driver.find_elements_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/text") and contains(@index, "1") and contains(@class, "android.widget.TextView")]')[1].click()

        waitSymbol = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/pound")]')
        actions = TouchAction(self.driver)
        actions.long_press(waitSymbol)
        actions.perform()
        print 'SUCCESS!: Symbol ; entered '
        #self.driver.find_elements_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/text") and contains(@index, "1") and contains(@class, "android.widget.TextView")]')[1].click()

        plusSymbol = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/zero")]')
        actions = TouchAction(self.driver)
        actions.long_press(plusSymbol)
        actions.perform()
        print 'SUCCESS!: Symbol + entered '
        #self.driver.find_elements_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/text") and contains(@index, "1") and contains(@class, "android.widget.TextView")]')[1].click()

        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/digits") and contains(@class, "android.widget.EditText")]')
        print textBox.text
        if (textBox.text == '0123456789*#,;+'):
            print 'SUCCESS!: Each digital input can be accurately after 0'
        else:
            print 'ERROR!: '
            raise

        self.driver.back()

    def testcase_smartDialEnterNumbers(self):
        self.open_app()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("3462", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("3462", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


    