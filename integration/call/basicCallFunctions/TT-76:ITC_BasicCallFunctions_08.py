# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.dialer'
        desired_caps['appActivity'] = 'com.android.dialer.DialtactsActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_app(self):
        sleep(5)
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/floating_action_button") and contains(@class, "android.widget.ImageButton")]').click()
        
        print 'SUCCESS!: Successfully enter the SYSTEM dialing interface'
        sleep(3)
        self.dial_number()

    def dial_number(self):
        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/digits") and contains(@class, "android.widget.EditText")]')
        textBox.clear()
        textBox.send_keys("52335689555")
        x = self.driver.find_elements_by_class_name('android.view.View')
        y = len(x)
        print y

        while y > 7:
            sleep(3)
            #self.driver.find_elements_by_class_name('android.widget.ImageButton')[0].click()
            #sleep(5)
            self.driver.tap([(75,121)], 400)
            
            self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.ImageButton")]').click()
            sleep(3)
            
            #self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/title") and contains(@class, "android.widget.TextView") and contains(@text, "Delete")]')
            self.driver.tap([(481,411)], 400)
            sleep(3)

            self.driver.tap([(570,712)], 400)
            #self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/button1") and contains(@class, "android.widget.Button") and contains(@text, "OK")]')
            sleep(3)

            print 'SUCCESS!: successfully deleted the contact number from the phone memory'
            textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/digits") and contains(@class, "android.widget.EditText")]')
            textBox.clear()
            textBox.send_keys("52335689555")
            y = y - 1
            print 'deleted contact'
  
        self.add_to_contacts()

    def add_to_contacts(self):
        try:
            self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.dialer:id/cliv_name_textview") and contains(@text, "Add to contacts")]').click()
            sleep(2)
            self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.contacts:id/cliv_name_textview") and contains(@text, "Create new contact")]').click()
            self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.contacts:id/account_type") and contains(@class, "android.widget.TextView")]').click()

            self.driver.find_element_by_xpath('//*[contains(@resource-id, "android:id/text1") and contains(@text, "PHONE")]').click()
            name = self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.EditText") and contains(@text, "Name")]')
            name.send_keys("Terminal")
            lname = self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.EditText") and contains(@text, "Phonetic name")]')
            lname.send_keys("Testing")
            self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.contacts:id/save_menu_item") and contains(@class, "android.widget.ImageView")]').click()
            print 'SUCCESS!: Successfully saved to phone contacts'
        except:
            print 'ERROR!: '
            raise

        self.driver.back()

    def testcase_smartDialEnterNumbers(self):
        self.open_app()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("3474", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("3474", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit