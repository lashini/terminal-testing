# -*- coding: utf-8 -*-
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        #desired_caps['appActivity'] = ' '
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def change_settings(self):

        # self.driver.swipe(328, 11, 379, 746, 400)
        # sleep(3)
        # self.driver.swipe(328, 11, 379, 746, 400)
        # self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.systemui:id/settings_button") and contains(@class, "android.widget.ImageButton") and contains(@package, "com.android.systemui")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "More") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView") and contains(@package, "com.android.settings")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "Cellular networks") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView") and contains(@package, "com.android.settings")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "Preferred network type") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView") and contains(@package, "com.android.phone")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "GSM only") and contains(@resource-id, "android:id/text1") and contains(@class, "android.widget.CheckedTextView") and contains(@package, "com.android.phone")]').click()

        self.driver.back()
        self.driver.back()
    def testcase_addSpeedDialsSetting(self):
        self.change_settings()


    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    unittest.TextTestRunner(verbosity=2).run(suite)