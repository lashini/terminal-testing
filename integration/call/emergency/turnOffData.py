# -*- coding: utf-8 -*-
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        #desired_caps['appActivity'] = ' '
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def turn_off_mac_data(self):
        i= 1
        for i in range (1,6):
            try:
                self.driver.find_element_by_xpath('//*[contains(@text, "McWiLL Data") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView")]').click()
                McWillToggleBtn = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.settings:id/switch_widget") and contains(@class, "android.widget.Switch")]')                
                x = McWillToggleBtn.text
                print x
                if x == 'ON':
                    self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.settings:id/switch_widget") and contains(@class, "android.widget.Switch")]').click()
                    self.driver.find_element_by_xpath('//*[contains(@text, "Sure") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
                    self.driver.find_element_by_xpath('//*[contains(@text, "Yes") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
                    print 'SUCCESS!: Successfully truned off the McwiLL data'
                    self.driver.back()
                    #self.driver.back()
                else:
                    print 'McWiLL data is off'
                    self.driver.back()
                    
                i = i + 9
                return i
            except:
                sleep(2)
                i = i + 1

            if i is 6:
                print 'ERROR!: Please check again'
                raise

    def turn_off_mobile_data(self):
        i= 1
        for i in range (1,6):
            try:
                self.driver.find_element_by_xpath('//*[contains(@text, "Data usage") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView")]').click()
                McWillToggleBtn = self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.Switch")]')                
                x = McWillToggleBtn.text
                print x
                if x == 'ON':
                    self.driver.find_element_by_xpath('//*[contains(@class, "android.widget.Switch")]').click()
                    self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
                    #self.driver.find_element_by_xpath('//*[contains(@text, "Yes") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
                    print 'SUCCESS!: Successfully truned off the Mobile data'
                    self.driver.back()

                else:
                    print 'Mobile data is off'
                    self.driver.back()
                    
                i = i + 9
                return i
            except:
                sleep(2)
                i = i + 1

            if i == 6:
                print 'Mobile data is off'
                self.driver.back()


    # def trun_off_wifi_data(self):
    #     i= 1
    #     for i in range (1,6):
    #         try:
    #             self.driver.find_element_by_xpath('//*[contains(@text, "WiFi") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView")]').click()
    #             McWillToggleBtn = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.settings:id/switch_widget") and contains(@class, "android.widget.Switch")]')                
    #             x = McWillToggleBtn.is_enabled()
    #             if x is True:
    #                 self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.settings:id/switch_widget") and contains(@class, "android.widget.Switch")]').click()
    #                 #self.driver.find_element_by_xpath('//*[contains(@text, "Yes") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
    #                 print 'SUCCESS!: Succewssfully truned off the WiFi data'
    #                 self.driver.back()

    #             else:
    #                 print 'WiFi data is off'
    #                 self.driver.back()
                    
    #             i = i + 9
    #             return i
    #         except:
    #             sleep(2)
    #             i = i + 1

    #         if i == 6:
    #             print 'ERROR!: Please check again'
    #             raise


    

    def testcase_addSpeedDialsSetting(self):
        self.turn_off_mac_data()
        self.turn_off_mobile_data()


    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    unittest.TextTestRunner(verbosity=2).run(suite)