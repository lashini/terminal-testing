# -*- coding: utf-8 -*-
import testlink
import os, sys
import glob
import unittest
from appium import webdriver
from time import sleep


PLATFORM_VERSION = '5.1.1'


class EntranceTests(unittest.TestCase):

    def setUp(self):
        print 'commandline args',sys.argv[1]
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'    
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.phone'
        desired_caps['appActivity'] = '.EmergencyDialer' 
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])
        self.driver = webdriver.Remote(url, desired_caps)


    def emergency_dial(self):
        #num 1
        print 'Calling 911'
        emergNum = self.driver.find_element_by_id('com.android.phone:id/digits')
        emergNum.clear()
        emergNum.send_keys("911")
        dial = self.driver.find_element_by_id('com.android.phone:id/floating_action_button')
        dial.click()
        sleep(10)
        # cancelcall = self.driver.find_element_by_id('com.android.dialer:id/floating_end_call_action_button')
        # cancelcall.click()
        i = 0
        for i in range(0,12):
            try:
                self.driver.find_element_by_id('com.android.phone:id/digits').is_displayed()
                print 'SUCCESS! Emergency Dialer: Successfully hanged out.'
                i = 20
                return i

            except Exception as e:
                i = i + 1
            
            if i is 12:
                raise
            else:
                sleep(12)
                
            pass

        sleep(3)

        self.driver.back()
        
    def testcase_emergencyDial(self):
        self.emergency_dial()  

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(EntranceTests)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    print result
   
    #pass -->       <unittest.runner.TextTestResult run=1 errors=0 failures=0>
    #fail -->       <unittest.runner.TextTestResult run=1 errors=1 failures=0>

    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("2016", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
            print 'PASSED !'
        else:
            tls.reportTCResult("2016", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)
            print 'FAILED !'
    except:
        exit

