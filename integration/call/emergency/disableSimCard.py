# -*- coding: utf-8 -*-
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.settings'
        desired_caps['appActivity'] = '.Settings'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def disable_sim_card(self):
        i= 1
        for i in range (1,6):
            try:
                self.driver.find_element_by_xpath('//*[contains(@text, "SIM cards") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView")]').click()
                McWillToggleBtn = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.settings:id/sub_switch_widget") and contains(@class, "android.widget.Switch")]')                
                x = McWillToggleBtn.text
                print x
                if x == 'ON':
                    McWillToggleBtn.click()
                    self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button1") and contains(@class, "android.widget.Button")]').click()
                    sleep(8)
                    self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button3") and contains(@class, "android.widget.Button")]').click()
                    print 'SUCCESS!: Successfully disabled sim card'
                    self.driver.back()
                else:
                    print 'Sim card is already disabled'
                    
                i = 9
                return i
            except:
                i = i + 1
            if i == 6:
                print 'ERROR!: Please check again'
                raise


    def testcase_addSpeedDialsSetting(self):
        self.disable_sim_card()

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    unittest.TextTestRunner(verbosity=2).run(suite)