# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        # desired_caps['appPackage'] = 'com.android.dialer'
        # desired_caps['appActivity'] = 'com.android.dialer.DialtactsActivity'
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'

        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_system_dialer(self):
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.camtalk.start:id/title") and contains(@class, "android.widget.TextView") and contains(@text, "Calls")]').click()

        v = 1
        while v < 7:
            try:
                textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
                textBox.clear()
                textBox.send_keys("000000*")
                self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
            except:
                self.driver.tap([(361,1111)], 400)
            if v < 6:
                try:
                    self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/floating_action_button") and contains(@class, "android.widget.ImageButton")]').click()
                    sleep(3)
                except:
                        pass

                self.call_number(v)

                self.driver.find_element_by_id("com.android.dialer:id/dialpad_floating_action_button").click()
                print 'Calling no : ' , v
                sleep(5)
                self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
                sleep(6)

            else:
                self.display_call_log()

            v = v + 1

    def call_number(self,v):
        if v == 1:
            textBox = self.driver.find_element_by_id("com.android.dialer:id/digits")
            textBox.clear()
            textBox.send_keys("0715555500")

        elif v == 2:
            textBox = self.driver.find_element_by_id("com.android.dialer:id/digits")
            textBox.clear()
            textBox.send_keys("07153213232")
        elif v == 3 :
            textBox = self.driver.find_element_by_id("com.android.dialer:id/digits")
            textBox.clear()
            textBox.send_keys("0416544465454")

        elif v == 4:
            textBox = self.driver.find_element_by_id("com.android.dialer:id/digits")
            textBox.clear()
            textBox.send_keys("64646234413")

        elif v == 5:
            textBox = self.driver.find_element_by_id("com.android.dialer:id/digits")
            textBox.clear()
            textBox.send_keys("664643467965")
        else:
            print 'ERROR!: Out of range'

    def display_call_log(self):
        sleep(2)
        self.driver.find_element_by_xpath('//*[contains(@text,"Recents") and contains(@class, "android.widget.TextView")]').click()
        sleep(8)
        x = self.driver.find_elements_by_class_name('android.widget.LinearLayout')
        y = len(x)
        if x > 0:
            print 'SUCCESS!: Successfully enter the system dialing interface'
        else:
            raise
        sleep(3)
        self.driver.back()


    def testcase_systemDialOpen(self):
        self.open_system_dialer()

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1565", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1565", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit

