# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(3)
        self.enter_numbers()

    def enter_numbers(self):
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("0123456789")
        print 'SUCCESS!: Enter numbers'
        self.move_cursor()

    def move_cursor(self):
        self.driver.tap([(193,629)], 400)
        print 'SUCCESS!: Move the cursor to accurately position desired by the user'
        self.delete_numbers()

    def delete_numbers(self):
        i = 0
        for i in range(0,3):
            self.driver.find_element_by_id("com.camtalk.start:id/del_edit").click()
            i = i + 1

        print 'SUCCESS!: Numbers successfully remove intermediate position'
        self.enter_middle_numbers()

    def enter_middle_numbers(self):
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_6").click()
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_5").click()
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_4").click()
        
        print 'SUCCESS!: Symbols can be inserted in the middle of the digital number'
        self.driver.back()

    def testcase_smartDialCursorMovements(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1510", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1510", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


#