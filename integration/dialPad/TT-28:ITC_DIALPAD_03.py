# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(3)
        self.enter_numbers()

    def enter_numbers(self):
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("0123456789")
        print 'SUCCESS!: Enter numbers'
        self.delete_numbers()

    def delete_numbers(self):
        i=0
        j = 10
        for i in range (0,10):
            j = j - 1
            l = str(j)
            self.driver.find_element_by_id("com.camtalk.start:id/del_edit").click()
            print 'SUCCESS!: Deleted digit '+ l +' from last'
            i = i + 1
        
        print 'SUCCESS!: Successfully delete numbers one by one from the last position'

        self.driver.back()

    def testcase_smartDialDeleteLastNumbers(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1507", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1507", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit




#