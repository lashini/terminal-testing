import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(3)
        self.enter_numbers()

    def enter_numbers(self):
        #1st contact
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("09912358455")
        self.driver.find_element_by_id("com.camtalk.start:id/tv_newcontact").click()
        fname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_first_name")
        fname.send_keys("kbbb")
        lname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_last_name")
        lname.send_keys("kkk")
        self.driver.find_element_by_xpath('//*[contains(@text,"Complete") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/right_text")]').click()


        #self.driver.keyevent(66)
        print 'SUCCESS!: added user 1: kkk user'

        #2nd contact
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("225136589")
        self.driver.find_element_by_id("com.camtalk.start:id/tv_newcontact").click()
        fname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_first_name")
        fname.send_keys("laaa")
        lname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_last_name")
        lname.send_keys("lLl")
        self.driver.find_element_by_xpath('//*[contains(@text,"Complete") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/right_text")]').click()

        #self.driver.keyevent(66)
        print 'SUCCESS!: added user 2: lLl user'

        #3rd contact
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("7412136978")
        self.driver.find_element_by_id("com.camtalk.start:id/tv_newcontact").click()
        fname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_first_name")
        fname.send_keys("Jaaa")
        lname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_last_name")
        lname.send_keys("JJJ")
        self.driver.find_element_by_xpath('//*[contains(@text,"Complete") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/right_text")]').click()

        #self.driver.keyevent(66)
        print 'SUCCESS!: added user 3: JJJ user'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("01111152116")
        self.driver.find_element_by_id("com.camtalk.start:id/tv_newcontact").click()
        fname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_first_name")
        fname.send_keys("aakk")
        lname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_last_name")
        lname.send_keys("aaajj")
        self.driver.find_element_by_xpath('//*[contains(@text,"Complete") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/right_text")]').click()


        #self.driver.keyevent(66)
        print 'SUCCESS!: added user 1: aakk aaakk'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("09912358455")
        self.driver.find_element_by_id("com.camtalk.start:id/tv_newcontact").click()
        fname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_first_name")
        fname.send_keys("bbjjj")
        lname = self.driver.find_element_by_id("com.camtalk.start:id/activity_add_contact_last_name")
        lname.send_keys("bblll")
        self.driver.find_element_by_xpath('//*[contains(@text,"Complete") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/right_text")]').click()


        #self.driver.keyevent(66)
        print 'SUCCESS!: added user 1: bkkbb bbkk'
        
        print 'SUCCESS!: successfully added to the phone contacts'
        self.driver.back()

    def testcase_dialPhone(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1536", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1536", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit



#