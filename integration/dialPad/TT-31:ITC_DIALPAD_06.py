# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(3)
        self.call_numbers()

    def call_numbers(self):
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("9992513624")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :1'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("0712014105")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :2'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("9996321136")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :3'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("9996984325")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :4'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("9991361023")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :5'

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.send_keys("9992223695")
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_g").click()
        sleep(2)
        try:
            self.driver.find_element_by_xpath('//*[contains(@text,"SIM Call") and contains(@resource-id, "com.camtalk.start:id/tv_call_type")]').click()            
        except:
            print 'Calling from default call methode'

        sleep(3)
        self.driver.find_element_by_id("com.android.dialer:id/floating_end_call_action_button").click()
        sleep(2)
        textBox.clear()
        print 'SUCCESS!: call no :6'

        self.search_by_num()

    def search_by_num(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Recent") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        self.driver.find_element_by_xpath('//*[contains(@class,"android.widget.EditText") and contains(@resource-id, "com.camtalk.start:id/contacts_et_search")]').click()
        #self.driver.find_element_by_xpath('//*[contains(@text,"Search:") and contains(@resource-id, "com.camtalk.start:id/contacts_et_search")]').click()
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/contacts_et_search")
        textBox.send_keys("136")
        self.driver.keyevent(66)
        sleep(5)
        self.count_matching()

    def count_matching(self):
        find_category_elements = self.driver.find_elements_by_class_name('XCUIElementTypeCell')
        total_categories = len(find_category_elements)
        print total_categories
        self.driver.find_element_by_xpath('//*[contains(@text,"0712014105")]').click()
        print 'SUCCESS!: Fast match to call log to start with 136'
        self.driver.back()

    def testcase_callMatchingCountByMultipleNumbers(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1518", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1518", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit



#