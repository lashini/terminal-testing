# -*- coding: utf-8 -*-
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.google.android.gms'
        desired_caps['appActivity'] = '.app.settings.GoogleSettingsActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_settings(self):
        i= 1
        for i in range (1,5):
            try:
                self.driver.find_element_by_xpath('//*[contains(@text, "Phone") and contains(@resource-id, "com.android.settings:id/title") and contains(@class, "android.widget.TextView")]').click()
                self.go_to_phone_setting()
                i = 5
            except:
                self.driver.swipe(351, 1150, 407, 290, 900)
                i = i + 1

        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        print 'SUCCESS!: Successfully enter the dialing interface'
        sleep(3)
        self.driver.back()

    def go_to_phone_setting(self):
        self.driver.find_element_by_xpath('//*[contains(@text, "General settings") and contains(@resource-id, "com.android.dialer:id/title") and contains(@class, "android.widget.TextView")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "Speed dial settings") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView")]').click()
        self.add_voicemail_no()
        self.add_speed_dial_number()
        self.driver.back()

    def add_voicemail_no(self):
        self.driver.find_element_by_xpath('//*[contains(@text, "Voicemail") and contains(@resource-id, "com.android.dialer:id/name") and contains(@class, "android.widget.TextView")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "Setup") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text, "Voicemail number") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView")]').click()
        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id, "android:id/edit") and contains(@class, "android.widget.EditText")]')
        textBox.clear()
        textBox.send_keys("0713435218")
        sleep(2)
        self.driver.keyevent(66)
        self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button2") and contains(@class, "android.widget.Button")]').click()    


    def add_speed_dial_number(self):
        self.driver.find_element_by_xpath('//*[contains(@index, "1") and contains(@class, "android.widget.TextView")]').click()
        #if there is already a speed dial number it will be deleted
        try:
            self.driver.find_element_by_xpath('//*[contains(@text, "Delete") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView")]').click()
        except:
            print ' '

        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.dialer:id/edit_container") and contains(@class, "android.widget.EditText")]')
        textBox.clear()
        textBox.send_keys("999999")
        sleep(2)
        self.driver.keyevent(66)
        self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button2") and contains(@class, "android.widget.Button")]').click()    


        self.driver.find_element_by_xpath('//*[contains(@index, "2") and contains(@class, "android.widget.TextView")]').click()
        #if there is already a speed dial number it will be deleted
        try:
            self.driver.find_element_by_xpath('//*[contains(@text, "Delete") and contains(@resource-id, "android:id/title") and contains(@class, "android.widget.TextView")]').click()
        except:
            print ' '

        textBox = self.driver.find_element_by_xpath('//*[contains(@resource-id, "com.android.dialer:id/edit_container") and contains(@class, "android.widget.EditText")]')
        textBox.clear()
        textBox.send_keys("88888")
        sleep(2)
        self.driver.keyevent(66)
        self.driver.find_element_by_xpath('//*[contains(@text, "OK") and contains(@resource-id, "android:id/button2") and contains(@class, "android.widget.Button")]').click()    

    def testcase_addSpeedDialsSetting(self):
        self.open_settings()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    unittest.TextTestRunner(verbosity=2).run(suite)