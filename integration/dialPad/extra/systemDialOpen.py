# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep
from appium.webdriver.common.touch_action import TouchAction

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.android.dialer'
        desired_caps['appActivity'] = 'com.android.dialer.DialtactsActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_app(self):
        sleep(5)
        self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/floating_action_button") and contains(@class, "android.widget.ImageButton")]').click()
        
        print 'SUCCESS!: Successfully enter the SYSTEM dialing interface'
        sleep(5)
        no1 = self.driver.find_element_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/one") and contains(@class, "android.widget.FrameLayout")]')
        actions = TouchAction(self.driver)
        actions.long_press(no1)
        actions.perform()
        
        self.driver.find_elements_by_xpath('//*[contains(@resource-id,"com.android.dialer:id/text") and contains(@index, "1") and contains(@class, "android.widget.TextView")]')[1].click()
        
        try:
            print 'llmllmlmlmlmlm'
            self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/button1") and contains(@text, "OK") and contains(@class, "android.widget.Button")]').click()   
            self.set_up_voicemail()
        except:
            print 'SUCCESS!: Successfully dialed the voice mail number'

    def set_up_voicemail(self):
        print 'a'           
        #self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/title") and contains(@text, "Setup") and contains(@class, "android.widget.TextView")  and contains(@package, "com.android.phone"]').click()   
        self.driver.tap([(197,375)], 400)
        print 'b'  
        #self.driver.find_element_by_xpath('//*[contains(@resource-id,"android:id/title") and contains(@text, "Voicemail number") and contains(@class, "android.widget.TextView")  and contains(@package, "com.android.phone"]').click()
        self.driver.tap([(584,506)], 400)
        self.driver.tap([(148,228)], 400)
        print 'c'  

        textBox = self.driver.find_element_by_id("android:id/edit")
        textBox.clear()
        textBox.send_keys("012345689")
        self.driver.keyevent(66)
        print 'giugvuigvu'
        self.driver.tap([(584,506)], 400)
        sleep(2)
        self.driver.tap([(584,753)], 400)

        #self.driver.find_element_by_xpath('//*[contains(@resource_id,"android:id/button2")and contains(@text, "OK") and contains(@class, "android.widget.Button") and contains(@package, "com.android.phone")]').click()
        print 'SUCCESS!: Set up voice mail number successfully'
        self.open_app()


    def testcase_systemDialOpen(self):
        self.open_app()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    # tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    # try:
    #     if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
    #         tls.reportTCResult("212", 1501 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
    #     else:
    #         tls.reportTCResult("212", 1501 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    # except:
    #     exit


