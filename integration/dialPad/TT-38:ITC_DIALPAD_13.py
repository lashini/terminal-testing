# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(1)
        self.enter_numbers()

    def enter_numbers(self):
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox.clear()
        textBox.send_keys("1234")
        sleep(2)
        print 'SUCCESS!: Successfully bring up the dialer, enter the number '
        self.device_wakeup()

    def device_wakeup(self):
        self.driver.lock()
        #sleep(2)
        self.driver.unlock()
        print 'here'
        #self.driver.swipe(351, 1237, 407, 290, 100)

        textBox1 = self.driver.find_element_by_xpath('//*[contains(@text,"1234") and contains(@resource-id, "com.camtalk.start:id/number")]')
        print 'After wake-up, the number has been entered will not be lost'
        self.continue_dialing()


    def continue_dialing(self):
        textBox2 = self.driver.find_element_by_id("com.camtalk.start:id/number")
        textBox2.click()
        textBox2.send_keys("0123")
        print 'SUCCESS!: Can continue to enter numbers accurately'

        self.driver.back()

    def testcase_deviceWakeUp(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1539", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1539", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


#