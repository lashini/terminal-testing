# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_dial_pad(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Calls") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        sleep(3)
        self.enter_numbers()

    def enter_numbers(self):
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_0").click()
        print 'SUCCESS!: Number 0 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_1").click()
        print 'SUCCESS!: Number 1 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_2").click()
        print 'SUCCESS!: Number 2 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_3").click()
        print 'SUCCESS!: Number 3 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_4").click()
        print 'SUCCESS!: Number 4 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_5").click()
        print 'SUCCESS!: Number 5 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_6").click()
        print 'SUCCESS!: Number 6 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_7").click()
        print 'SUCCESS!: Number 7 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_8").click()
        print 'SUCCESS!: Number 8 entered successfully'
        self.driver.find_element_by_id("com.camtalk.start:id/ibtn_key_9").click()
        print 'SUCCESS!: Number 9 entered successfully'

        print 'SUCCESS!: Each digital input can be accurately'

        self.driver.back()

    def testcase_smartDialEnterNumbers(self):
        self.open_dial_pad()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1504", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1504", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


    #