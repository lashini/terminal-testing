# -*- coding: utf-8 -*-
import testlink
import os,sys
import unittest
from appium import webdriver
from time import sleep

class Integration(unittest.TestCase):

    def setUp(self):

        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1.1'
        desired_caps['deviceName'] = 'CooTel S32'
        desired_caps['udid'] = sys.argv[1] 
        desired_caps['appPackage'] = 'com.camtalk.start'
        desired_caps['appActivity'] = 'com.uip.start.activity.MainActivity'
        url = "http://localhost:{}/wd/hub".format(sys.argv[2])

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def open_app(self):
        self.driver.find_element_by_xpath('//*[contains(@text,"Contacts") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/title")]').click()
        self.driver.find_element_by_xpath('//*[contains(@text,"All") and contains(@class, "android.widget.RadioButton") and contains(@resource-id, "com.camtalk.start:id/friend_rb_all")]').click()
        print 'SUCCESS!: Successfully viewed all the Contacts'
        sleep(3)
        x = False
        i = 0
        while x is False:
            for i in range(0,9):
                try:
                    self.driver.find_element_by_xpath('//*[contains(@index, "1") and contains(@class, "android.widget.RelativeLayout") and contains(@package, "com.camtalk.start")]').is_displayed()
                    x = True
                    i = 10
                    return x,i
                    pass
                    
                except:
                    self.driver.implicitly_wait(300)
                    i = i + 1

    def goto_search(self):
        self.driver.find_element_by_xpath('//*[contains(@index, "1") and contains(@class, "android.widget.RelativeLayout") and contains(@package, "com.camtalk.start")]').is_displayed()
        self.search_contacts()

    def search_contacts(self):
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/contacts_et_search")
        textBox.clear()
        textBox.send_keys("KkK")
        
        self.driver.keyevent(66)
        sleep(5)
        x = self.driver.find_elements_by_class_name("android.widget.RelativeLayout")
        print len(x)

        if x > 5:
            x = self.driver.find_element_by_xpath('//*[contains(@index, "2") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_contact_name")]')
            y = x.text

            z = y.lower()
            print z
            if z.find('kkk') != -1:
                print 'SUCCESS!: Search results diplyed Contacts contains KkK'
            else:
                raise  

        else:
            raise
        #x = self.driver.find_element_by_xpath('//*[contains(@index, "0") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_catlog")]')
        
        textBox = self.driver.find_element_by_id("com.camtalk.start:id/contacts_et_search")
        textBox.clear()
        textBox.send_keys("jJJ")
        
        self.driver.keyevent(66)
        sleep(5)
        x = self.driver.find_elements_by_class_name("android.widget.RelativeLayout")
        print len(x)

        if x > 5:
            x = self.driver.find_element_by_xpath('//*[contains(@index, "2") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_contact_name")]')
            y = x.text

            z = y.lower()
            print z
            if z.find('jjj') != -1:
                print 'SUCCESS!: Search results diplyed Contacts contains jJJ'
            else:
                raise  

        else:
            raise# Raise the exception that brought you here       
            

        textBox = self.driver.find_element_by_id("com.camtalk.start:id/contacts_et_search")
        textBox.clear()
        textBox.send_keys("lll")
        
        self.driver.keyevent(66)
        sleep(5)
        x = self.driver.find_elements_by_class_name("android.widget.RelativeLayout")
        print len(x)

        if x > 5:
            x = self.driver.find_element_by_xpath('//*[contains(@index, "2") and contains(@class, "android.widget.TextView") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_contact_name")]')
            y = x.text

            z = y.lower()
            print z
            if z.find('lll') != -1:
                print 'SUCCESS!: Search results diplyed Contacts contains KkK'
            else:
                raise  

        else:
            raise
            #self.driver.find_element_by_xpath('//*[contains(@text, "Z") and contains(@resource-id, "com.camtalk.start:id/fragment_contact_list_item_catlog")]').click()
    

        self.driver.back()

    def testcase_contactsNameMatch(self):
        self.open_app()
        self.goto_search()
    

    def tearDown(self):
        self.driver.quit()

    #def test_add_listings(self):
     #   self.driver.implicitly_wait(650)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Integration)
    result = str(unittest.TextTestRunner(verbosity=2).run(suite))

    tls = testlink.TestLinkHelper().connect(testlink.TestlinkAPIClient)
    try:
        if result == '<unittest.runner.TextTestResult run=1 errors=0 failures=0>':
            tls.reportTCResult("1530", 116 , 'TT_1.0', 'p', 'Test execution is passed on CooTel S32 device.', user='Lashini', platformid=1)
        else:
            tls.reportTCResult("1530", 116 , 'TT_1.0', 'f', 'Test execution is failed on CooTel S32 device.', user='Lashini', platformid=1)

    except:
        exit


#
